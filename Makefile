results: output/animprot_cancer_correlations.png output/plntprot_cancer_correlations.png

output/animprot_cancer_correlations.png: ch83.Rdata script/cancer_correlations.R
	Rscript script/cancer_correlations.R

output/plntprot_cancer_correlations.png: ch83.Rdata script/cancer_correlations.R
	Rscript script/cancer_correlations.R

ch83.Rdata: metadata.tsv data/CH83DG.CSV data/CH83M.CSV data/CH83PRU.CSV data/CH83Q.CSV
	Rscript script/prep_ch83.R

metadata.tsv: data/CHNAME.TXT
	ruby clean_chname.rb

retrieve_data: retrieve_text retrieve_data

retrieve_text: data/index.htm data/Mono_Foreword.pdf data/Mono_Study_Description.pdf data/Mono_Statistic_Summary.pdf data/Mono_Mortality.pdf data/Mono_Laboratory.pdf data/Mono_Diet_Survey.pdf data/Mono_Questionnaire_Surveys.pdf data/Mono_Questionnaires.pdf data/Mono_Annex.pdf data/chdata.htm

retrieve_data: data/CH83DG.CSV data/CH83M.CSV data/CH83PRU.CSV data/CH83Q.CSV data/CH89DG.CSV data/CH89M.CSV data/CH89PRU.CSV data/CH89Q.CSV data/CH93PRU.CSV data/CH93Q.CSV data/CHNAME.TXT data/CHTAIM.CSV data/CHTAIPRU.CSV data/CHTAIQ.CSV

data/%:
	mkdir -p data
	curl http://www.ctsu.ox.ac.uk/~china/monograph/$* > $@

clean:
	rm metadata.tsv
	rm ch83.Rdata
	rm output/*
